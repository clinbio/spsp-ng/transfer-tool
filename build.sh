#!/usr/bin/env bash

# abort on errors
set -e

# set colors for output
ESC_SEQ="\033["
COL_CYAN=${ESC_SEQ}"0;36m"
COL_RED=${ESC_SEQ}"0;31m"
COL_GREY=${ESC_SEQ}"1;30m"
BACK_CYAN=${ESC_SEQ}"0;30;46m"
BACK_RED=${ESC_SEQ}"0;30;41m"
BACK_GREEN=${ESC_SEQ}"0;30;42m"
COL_RESET=${ESC_SEQ}"39;49;00m"

if [ ! -f spsp ]; then
TIME=`date +"%T"` 
    printf "${BACK_RED} ERROR ${COL_RESET} ${COL_RED}Stopped with 1 error${COL_RESET} ${COL_GREY} ${TIME} ${COL_RESET}\n"
    printf "${BACK_RED} error ${COL_RESET} File not found: spsp script has not been found, please check your current directory '$(pwd)'."
    exit 2
else
    version=$(awk -F'"' '/^VERSION=/ {print $2}' spsp )
fi

if [ ! -f README.md ]; then
TIME=`date +"%T"` 
    printf "${BACK_RED} ERROR ${COL_RESET} ${COL_RED}Stopped with 1 error${COL_RESET} ${COL_GREY} ${TIME} ${COL_RESET}\n"
    printf "${BACK_RED} error ${COL_RESET} File not found: README.md has not been found, please check your current directory '$(pwd)'."
    exit 2
fi

if [ ! -f .pub ]; then
TIME=`date +"%T"` 
    printf "${BACK_RED} ERROR ${COL_RESET} ${COL_RED}Stopped with 1 error${COL_RESET} ${COL_GREY} ${TIME} ${COL_RESET}\n"
    printf "${BACK_RED} error ${COL_RESET} File not found: .pub (PGP SPSP public key) has not been found, please check your current directory '$(pwd)'."
    exit 2
fi

OUTBOX=".outbox"
if [ ! -d "$OUTBOX" ]; then
    mkdir $OUTBOX
fi

SENT=sent
if [ ! -d "$SENT" ]; then
    mkdir $SENT
fi

LOGS=logs
if [ ! -d "$LOGS" ]; then
    mkdir $LOGS
fi

VIRUSES=viruses
if [ ! -d "$VIRUSES" ]; then
    mkdir $VIRUSES
fi

BACTERIA=bacteria
if [ ! -d "$BACTERIA" ]; then
    mkdir $BACTERIA
fi


#Creating builds folder if necessary or clean it
if [ ! -d builds ]; then
    mkdir builds
else
    rm -rf ./builds/*
fi

echo "-  Archiving version ${version} of the transfer-tool..."
#Creating ZIP archive and test it
zip ./builds/transfer-toolv${version}.zip * .* -qq -x .git/ .gitignore .gitlab/ .gitlab-ci.yml .env ../ build.sh logs/** bacteria/** viruses/** builds/ .outbox/** sent/** .DS_Store
printf "${BACK_GREEN} DONE ${COL_RESET} Build archived. The ${COL_CYAN}zip${COL_RESET} archive has been created.\n"
#Creating TAR archive
tar cf ./builds/transfer-toolv${version}.tar --exclude='.DS_Store' --exclude='.git' --exclude='.gitlab' --exclude='.gitignore' --exclude='.gitlab-ci.yml' --exclude='.env' --exclude='build.sh' --exclude "logs/*.*" --exclude='bacteria/*.*' --exclude='viruses/*.*' --exclude='.outbox/*.*' --exclude='sent/*.*' --exclude='builds' .
printf "${BACK_GREEN} DONE ${COL_RESET} Build archived. The ${COL_CYAN}tar${COL_RESET} archive has been created.\n"
#Creating TAR.GZ archive
tar cfz ./builds/transfer-toolv${version}.tar.gz --exclude='.DS_Store' --exclude='.git' --exclude='.gitlab' --exclude='.gitignore' --exclude='.gitlab-ci.yml' --exclude='.env' --exclude='build.sh' --exclude "logs/*.*" --exclude='bacteria/*.*' --exclude='viruses/*.*' --exclude='.outbox/*.*' --exclude='sent/*.*' --exclude='builds' .
printf "${BACK_GREEN} DONE ${COL_RESET} Build archived. The ${COL_CYAN}tar.gz${COL_RESET} archive has been created.\n"
#Creating TAR.BZ2 archive
tar cfj ./builds/transfer-toolv${version}.tar.bz2 --exclude='.DS_Store' --exclude='.git' --exclude='.gitlab' --exclude='.gitignore' --exclude='.gitlab-ci.yml' --exclude='.env' --exclude='build.sh' --exclude "logs/*.*" --exclude='bacteria/*.*' --exclude='viruses/*.*' --exclude='.outbox/*.*' --exclude='sent/*.*' --exclude='builds' .
printf "${BACK_GREEN} DONE ${COL_RESET} Build archived. The ${COL_CYAN}tar.bz2${COL_RESET} archive has been created.\n"

printf "${BACK_GREEN} DONE ${COL_RESET} All build archived. The ${COL_CYAN}zip, tar, tar.gz and tar.gz2${COL_RESET} archives has been created.\n"
printf "${BACK_CYAN} INFO ${COL_RESET} Check out official releases at ${COL_CYAN}https://gitlab.sib.swiss/SPSP/transfer-tool/-/releases${COL_RESET}\n\n\n"

exit 0