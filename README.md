#  SPSP Transfer Tool 

[![pipeline status](https://gitlab.sib.swiss/SPSP/transfer-tool/badges/master/pipeline.svg)](https://gitlab.sib.swiss/SPSP/transfer-tool/-/commits/master)

WARNING:
NO LONGER MAINTAINED
Please note that the SPSP Transfer Tool was fully re-factored and is now available as the SendCrypt tool either on the [command line](https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli) or through a graphical [user interface](https://sendcrypt.sib.swiss/). Please find all documentation [here](https://clinbiokb.sib.swiss/s/sendcrypt).


The aim of the Transfer Tool (TT) is to provide a tool allowing users of the Swiss Pathogen Surveillance Platform to easily and securely transfer sequencing files. The TT is a simple shell script relying on multiple libraries (such as GPG and OpenSSH) to archive, hash, encrypt and transfer FASTQ files with their metadata.

# Documentation
https://gitlab.sib.swiss/SPSP/transfer-tool/-/wikis/home

# License
The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.

# Reference
This tool is part of the Swiss Pathogen Surveillance Platform

# Contact
SPSP Support
[spsp-support@sib.swiss](mailto:spsp-support@sib.swiss)
